local keymap = vim.keymap

keymap.set('n', 'x', '"_x')

-- Increment / Decrement
keymap.set('n', '+', '<C-a>')
keymap.set('n', '-', '<C-x>')

keymap.set('n','dw','vb"_d')

keymap.set('n', '<C-a>', 'gg<S-v>G')

keymap.set('n','te', ':tabedit<Return>')

keymap.set('n','ss', ':split<Return><C-w>w')
keymap.set('n','sv', ':vsplit<Return><C-w>w')

keymap.set('n','<Space>', '<C-w>w')
