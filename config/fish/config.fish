if status is-interactive
    # Commands to run in interactive sessions can go here
    set -gx GPG_TTY (tty)
    alias ls="exa --all --long --icons --git"
    alias vim="nvim"
end
